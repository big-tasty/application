'use strict';

window.backend = (function () {
    const ROUTES = {
        "/points": ROUTE_POINTS,
        "/borders": ROUTE_CLUSTERS
    };

    return (url) => new Promise((resolve, reject) => {
        const data = ROUTES[url];
        return data ? resolve(data) : reject({status: 404, message: "Not found"});
    });
})();
