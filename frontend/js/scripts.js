'use strict';

var map;
var heatmap;

function initMap() {
    backend("/borders").then(drawHeatMap);

    backend("/borders").then(drawPoints);

    const saintPetersburg = {lat: 59.9342802, lng: 30.3350986};
    map = new google.maps.Map(
        document.querySelector('.map'),
        {center: saintPetersburg, zoom: 8}
    );
}

function drawHeatMap(data) {
    heatmap = new google.maps.visualization.HeatmapLayer({
        data: data.map(({lat, lng}) => new google.maps.LatLng(lat, lng)),
        dissipating: false,
        map: map
    });
}

function drawPoints(data) {
    data
        .reduce((areas, point) => {
            let area = areas[point.area] || [];
            area.push(point);
            areas[point.area] = area;
            return areas;
        }, [])
        .map(area => new google.maps.Polygon({
            paths: area,
            strokeColor: 'rgba(255, 95, 60, 1)',
            strokeOpacity: 0.8,
            strokeWeight: 2,
            fillColor: 'rgba(255, 95, 60, 0.5)',
            fillOpacity: 0.35
        }))
        .forEach(area => area.setMap(map));
}
