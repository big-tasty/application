package com.epam.hackathon.service;

import org.junit.Test;

/**
 * @author (created on 12/3/2017).
 */
public class ScriptRunnerTest {
    @Test
    public void runScriptKMeans() throws Exception {

        ScriptRunner.runScriptKMeans();
    }

    @Test
    public void runScriptDbscan() throws Exception {

        ScriptRunner.runScriptDbscan();
    }

}