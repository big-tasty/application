import pandas as pd
import numpy as np
from sklearn.cluster import DBSCAN
import json

df = pd.read_csv('data/MVD_News/news_with_location.csv')
X = df[['lat', 'lng']]

dbscan = DBSCAN(min_samples = 20, eps = 3)
dbscan.fit(X)

n_clusters = len(set(dbscan.labels_)) - 1
df['area']=dbscan.labels_

points = df[['lat', 'lng', 'area']]
out = points.to_json(orient='records')

c = open('data.json', 'r+')
c.truncate()
c.close()
f = open('data.json', 'a')
f.writelines(out)
f.close()
