import pandas as pd
import numpy as np
from IPython.display import display
from sklearn.cluster import KMeans
import math
import sys

k = 10
k = sys.argv[0]

df = pd.read_csv('news_with_location.csv')
X = df[['lat', 'lng']]

kmeans = KMeans(n_clusters=k, random_state=241)
kmeans.fit(X)
df['area']=kmeans.labels_
i = 0
points = pd.DataFrame(columns=['lat', 'lng', 'area'])
while i < k:
    area = df.loc[df['area'] == i]
    if len(area) > 5:
        latMin = area['lat'].min()
        latMax = area['lat'].max()
        step = (latMax - latMin) / resolution
        lat = latMin

        while lat <= latMax:
            crange = area[(area.lat >= lat) & (area.lat < lat + step)]
            crangeLngMax = crange['lng'].max()
            if math.isnan(crangeLngMax) == False:
                line = crange.loc[crange['lng'] == crangeLngMax]
                point = line[['lat', 'lng', 'area']].iloc[[0]]
                frames = [points, point]
                points = pd.concat(frames)
            lat = lat + step
            
        lat = latMax
        while lat >= latMin:
            crange = area[(area.lat < lat) & (area.lat >= lat - step)]
            crangeLngMin = crange['lng'].min()
            if math.isnan(crangeLngMin) == False:
                line = crange.loc[crange['lng'] == crangeLngMin]
                point = line[['lat', 'lng', 'area']].iloc[[0]]
                frames = [points, point]
                points = pd.concat(frames)
            lat = lat - step
    i = i + 1 
out = points.to_json(orient='records')

c = open('data.json', 'r+')
c.truncate()
c.close()
f = open('data.json', 'a')
f.writelines(out)
f.close()
