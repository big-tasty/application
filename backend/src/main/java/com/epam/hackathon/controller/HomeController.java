package com.epam.hackathon.controller;

import com.epam.hackathon.service.ScriptRunner;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Path("/")
@RestController
public class HomeController {

    private static final String outputFile = "D:\\1_Workspaces\\github\\hack\\application\\backend\\src\\main\\resources\\data.json";

    @GET
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    public String index() {

        return "index";
    }

    @GET
    @Path("/points")
    @Produces(MediaType.APPLICATION_JSON)
    public String points() throws IOException {

        final int runScript = ScriptRunner.runScriptKMeans();

        StringBuilder stringBuilder = new StringBuilder();
        if (runScript == 0) {
            Files.lines(Paths.get(outputFile))
                    .forEach(stringBuilder::append);
        }

        return stringBuilder.toString();

    }


}
