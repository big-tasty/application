package com.epam.hackathon.service;

import org.zeroturnaround.exec.ProcessExecutor;
import org.zeroturnaround.exec.ProcessResult;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @author (created on 12/3/2017).
 */
public class ScriptRunner {

    private static final String PYTHON = "python";
    private static final String KM_PARAM = String.join(File.pathSeparator,"src", "main", "resources", "km_param.py");
    private static final String DB_SCAN = String.join(File.pathSeparator,"src", "main", "resources", "dbscan.py");

    public static int runScriptKMeans() {
        final ProcessExecutor processExecutor = new ProcessExecutor();
        processExecutor.command(PYTHON, KM_PARAM, "10");
        processExecutor.redirectOutput(System.out);
        try {
            final ProcessResult execute = processExecutor.execute();

            if (execute.getExitValue() == 0) {
                System.out.println("good job guys");
            } else {
                System.out.println("bad");
            }
        } catch (IOException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public static int runScriptDbscan() {
        final ProcessExecutor processExecutor = new ProcessExecutor();
        processExecutor.command(PYTHON, DB_SCAN);
        processExecutor.redirectOutput(System.out);
        try {
            final ProcessResult execute = processExecutor.execute();

            if (execute.getExitValue() == 0) {
                System.out.println("good job guys");
            } else {
                System.out.println("bad");
            }
        } catch (IOException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        return 0;

    }
}
