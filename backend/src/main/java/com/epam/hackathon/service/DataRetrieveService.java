package com.epam.hackathon.service;

import au.com.bytecode.opencsv.CSVReader;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Slf4j
public class DataRetrieveService {

    public static List<String> filterData(List<String> offences) {
        return offences.stream()
                .map(String::toLowerCase)
                .filter(s -> s.contains("убийств") ||
                        s.contains("кража") ||
                        s.contains("насил") ||
                        s.contains("оружи") ||
                        s.contains("боеприпас") ||
                        s.contains("мошенничеств") ||
                        s.contains("уничтожени") ||
                        s.contains("похищени") ||
                        s.contains("покушени") ||
                        s.contains("грабеж"))
                .collect(Collectors.toList());

    }

    public static List<String> getOffence(String fileName) {
        log.info("Starting parsing offences");
        Pattern pattern = Pattern.compile("\\(([^)]+)\\)");
        Matcher matcher;
        List<String> offences = new ArrayList<>();
        try (CSVReader reader = new CSVReader(new FileReader(fileName))) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                log.info("Line is {}", line[5]);
                matcher = pattern.matcher(line[5]);
                if (matcher.find()) {
                    log.info("Line that matches is {}", line[5]);
                    offences.add(matcher.group(1));
                }
            }
        } catch (IOException e) {
            log.error("Parsing of csv file failed");
        }
        return offences;
    }
}
