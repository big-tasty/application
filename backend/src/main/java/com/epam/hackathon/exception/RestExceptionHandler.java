package com.epam.hackathon.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

@Slf4j
public abstract class RestExceptionHandler {

    private final Class<? extends RestExceptionHandler> className = getClass();

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public String handleError(final Exception exception, final HttpServletResponse httpServletResponse) {
        log.error("An exception through execute " + className + " controller vith exception ");
        exception.printStackTrace();
        final String name_ex = exception.getClass().getName();
        if (name_ex.equals("org.springframework.web.bind.MissingServletRequestParameterException") ||
                name_ex.equals("org.springframework.web.bind.ServletRequestBindingException") ||
                name_ex.equals("org.springframework.web.bind.UnsatisfiedServletRequestParameterException")) {
            httpServletResponse.setStatus(HttpStatus.BAD_REQUEST.value());
        } else {
            httpServletResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        }
        return String.valueOf(exception.getMessage());
    }
}
