package com.epam.hackathon.model;

import lombok.EqualsAndHashCode;
import lombok.ToString;

@EqualsAndHashCode
@ToString
public class Offence {
    private final String name;

    public Offence(String name) {
        this.name = name;
    }
}
