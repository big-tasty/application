package com.epam.hackathon.model;

import java.util.stream.Stream;

public enum OffenceType {
    STEAL("кража"),
    MURDER("убиство"),
    ROBBER("грабёж"),
    DRUGS("наркотики"),
    RAPE("изнасилование"),
    WEAPONS("оружие"),
    OTHER("остальное");

    private final String status;

    OffenceType(String status) {
        this.status = status;
    }

//    public static OffenceType identify(final String undefinedStatus) {
//        return Stream.of(OffenceType.values())
//                .filter(changeType -> changeType.status.equals(unDefinedStatus))
//                .findAny()
//                .orElse(ChangeType.UNKNOWN);
//    }
}
