package com.epam.hackathon.model;

import java.util.LinkedList;

public class Area {
    public final LinkedList<LatLongRest> latLongRests;

    public Area(LinkedList<LatLongRest> latLongRests) {
        this.latLongRests = latLongRests;
    }
}
