package com.epam.hackathon.model;

public class LatLongRest {

    private final double lat;
    private final double lng;

    public LatLongRest(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }
}
